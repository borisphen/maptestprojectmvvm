package mapapp.bender.com.placesapp.di.module;


import android.arch.persistence.room.Room;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import mapapp.bender.com.placesapp.Application;
import mapapp.bender.com.placesapp.repository.db.AppDatabase;

@Module
public class DatabaseModule {

    private static final String DB_NAME = "places_db";

    @Provides
    @Singleton
    AppDatabase provideDatabase() {
        return Room.databaseBuilder(Application.getInstance(), AppDatabase.class, DB_NAME)
                .fallbackToDestructiveMigration()
                .build();
    }
}
