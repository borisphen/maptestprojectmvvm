package mapapp.bender.com.placesapp.ui.map;

import android.app.Activity;
import android.databinding.DataBindingUtil;
import android.text.TextUtils;
import android.view.View;

import com.bender.placesapp.R;
import com.bender.placesapp.databinding.InfoItemBinding;
import com.bender.placesapp.databinding.InfoWindowBinding;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.Marker;

import mapapp.bender.com.placesapp.repository.data.entities.places.Item;
import mapapp.bender.com.placesapp.utils.JSONUtils;


public class InfoAdapter implements GoogleMap.InfoWindowAdapter {

    private Activity context;
    private Item chosenItem;

    InfoAdapter(Activity context) {
        this.context = context;
    }

    private InfoAdapter() {
    }

    @Override
    public View getInfoWindow(Marker marker) {
        InfoWindowBinding binding = DataBindingUtil.inflate(context.getLayoutInflater(),
                R.layout.info_window, null, false);
        if (!TextUtils.isEmpty(marker.getSnippet())){
            binding.setModel(new PlaceBindingModel(JSONUtils.getObjectFromString(marker.getSnippet(), Item.class)));
        } else {
            binding.setModel(new PlaceBindingModel(chosenItem));
        }
        binding.executePendingBindings();
        return binding.getRoot();
    }

    @Override
    public View getInfoContents(Marker marker) {
        return null;
    }

    public void setChosenClusterItem(Item item){
        chosenItem = item;
    }
}
