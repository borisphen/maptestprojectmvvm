package mapapp.bender.com.placesapp.di.module;

import dagger.Module;
import dagger.Provides;
import mapapp.bender.com.placesapp.di.scope.PerActivity;
import mapapp.bender.com.placesapp.ui.map.MapsActivity;

@Module
public class ActivityModule {

    private final MapsActivity mapsActivity;

    public ActivityModule(MapsActivity mapsActivity) {
        this.mapsActivity = mapsActivity;
    }

    @Provides
    @PerActivity
    public MapsActivity provideContext(){
        return mapsActivity;
    }
}
