package mapapp.bender.com.placesapp.repository.db;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

import javax.inject.Singleton;

import mapapp.bender.com.placesapp.repository.data.entities.places.Item;
import mapapp.bender.com.placesapp.repository.db.daos.PlacesDao;

@Singleton
@Database(entities = {
        Item.class
}, version = 1)
public abstract class AppDatabase extends RoomDatabase {
    public abstract PlacesDao getPlacesDao();
}