package mapapp.bender.com.placesapp.repository.data.entities.places;

public interface Place {

    double getLatitude();

    double getLongitude();

    String getAddress();

    int getRate();

    int getDistanceToMe();

    String getOwnerFullName();

    int getPrice();

    String getPicture();

    void setOwner(Owner owner);

    void setAddress(String address);

    String getName();

    boolean isFavorite();

}
