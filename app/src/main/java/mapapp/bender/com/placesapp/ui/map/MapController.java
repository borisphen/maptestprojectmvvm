package mapapp.bender.com.placesapp.ui.map;

import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.maps.android.clustering.Cluster;
import com.google.maps.android.clustering.ClusterItem;
import com.google.maps.android.clustering.ClusterManager;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Nullable;

import mapapp.bender.com.placesapp.repository.data.entities.places.Item;

public class MapController {

    private static int MAP_PADDINGS = 10;
    private ClusterManager<Item> clusterManager;
    private GoogleMap map;
    private OnClusterClickListener clusterClickListener;
    private MapsActivity mapsActivity;
    private Cluster<Item> clickedCluster;
    private InfoAdapter clusterInfoAdapter;

    public interface OnClusterClickListener {
        void OnClusterClick(List<Item> items);
    }

    public MapController(GoogleMap map, OnClusterClickListener clusterClickListener, MapsActivity mapsActivity) {
        this.clusterManager = new ClusterManager<>(mapsActivity, map);
        this.map = map;
        this.clusterClickListener = clusterClickListener;
        this.mapsActivity = mapsActivity;
        clusterInfoAdapter = new InfoAdapter(mapsActivity);
    }

    public void setupClusters(List<Item> items) {
        map.setOnCameraIdleListener(clusterManager);
        map.setOnMarkerClickListener(clusterManager);
        clusterManager.getMarkerCollection().setOnInfoWindowAdapter(new InfoAdapter(mapsActivity));
        clusterManager.getClusterMarkerCollection().setOnInfoWindowAdapter(clusterInfoAdapter);

        clusterManager.setOnClusterItemClickListener(item -> {
            Toast.makeText(mapsActivity, "Clicked individual Marker: " + item.getName(),
                    Toast.LENGTH_SHORT).show();
            return false;
        });
        clusterManager.setOnClusterClickListener(cluster -> {
            clickedCluster = cluster;
            ArrayList<Item> clusterItems = new ArrayList<>(cluster.getItems());
            moveCameraToPosition(cluster.getPosition());
            clusterClickListener.OnClusterClick(clusterItems);
            return true;
        });

        map.setInfoWindowAdapter(clusterManager.getMarkerManager());
        clusterManager.addItems(items);
        clusterManager.cluster();
        positioningCameraBounds(items);
    }

    private void positioningCameraBounds(List<Item> items) {
        LatLngBounds.Builder builder = new LatLngBounds.Builder();
        for (Item item : items) {
            builder.include(item.getPosition());
        }
        LatLngBounds bounds = builder.build();
        CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, MAP_PADDINGS);
        map.moveCamera(cu);
    }

    void showInfoWindow(Item item) {
        clusterInfoAdapter.setChosenClusterItem(item);
        map.moveCamera(CameraUpdateFactory.newLatLng(item.getPosition()));
        Marker marker = getMarkerByPosition();
        if (marker != null) {
            moveCameraToPosition(clickedCluster.getPosition());
            marker.showInfoWindow();
        }
    }

    @Nullable
    private Marker getMarkerByPosition() {
        for (Marker marker : clusterManager.getClusterMarkerCollection().getMarkers()) {
            if (marker.getPosition().equals(clickedCluster.getPosition())) {
                return marker;
            }
        }
        return null;
    }

    private void moveCameraToPosition(LatLng latLng){
        map.animateCamera(CameraUpdateFactory.newLatLng(latLng));
    }
}
