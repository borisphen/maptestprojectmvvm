package mapapp.bender.com.placesapp.repository.network;

import java.util.List;

import io.reactivex.Observable;
import mapapp.bender.com.placesapp.repository.data.entities.places.Owner;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface Api {
    @GET("/api/?region=england&")
    Observable<List<Owner>> getOwner(@Query("amount") int amount);
}
