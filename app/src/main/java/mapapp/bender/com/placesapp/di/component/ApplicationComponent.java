package mapapp.bender.com.placesapp.di.component;

import javax.inject.Singleton;

import dagger.Component;
import mapapp.bender.com.placesapp.Application;
import mapapp.bender.com.placesapp.di.module.ActivityModule;
import mapapp.bender.com.placesapp.di.module.ApplicationModule;
import mapapp.bender.com.placesapp.di.module.DatabaseModule;
import mapapp.bender.com.placesapp.di.module.NetworkModule;

@Singleton
@Component(modules = {ApplicationModule.class, NetworkModule.class, DatabaseModule.class})
public interface ApplicationComponent {
    void inject(Application application);
    ActivitySubComponent plus(ActivityModule activityModule);
}
