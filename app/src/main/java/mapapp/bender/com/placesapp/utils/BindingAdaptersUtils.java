package mapapp.bender.com.placesapp.utils;

import android.content.Context;
import android.databinding.BindingAdapter;
import android.databinding.InverseBindingMethod;
import android.databinding.InverseBindingMethods;
import android.graphics.drawable.Drawable;
import android.support.v4.widget.DrawerLayout;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.maps.GoogleMap;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.util.List;

import mapapp.bender.com.placesapp.repository.data.entities.places.Item;
import mapapp.bender.com.placesapp.ui.map.MapController;

/**
 * Utility for binding adapters
 */
public class BindingAdaptersUtils {

    @BindingAdapter({"imagePath"})
    public static void loadImage(ImageView view, String imagePath) {
        Picasso.with(view.getContext()).load(imagePath)
                .resize(150, 100)
                .into(view);
//        ImageLoaderUtils.asyncLoadImageIntoImageViewFromURL(view.getContext(), view, imagePath);
    }

    @BindingAdapter("visible")
    public static void setVisibility(View v, boolean isVisible) {
        if (isVisible) {
            v.setVisibility(View.VISIBLE);
        } else {
            v.setVisibility(View.GONE);
        }
    }

    @BindingAdapter({"srcCompat"})
    public static void srcCompat(ImageView view, Drawable drawableResource) {
        view.setImageDrawable(drawableResource);
    }


//    @BindingAdapter({"initMap"})
//    public static void initMap(GoogleMap map, List<Item> places, Context context) {
//        MapController.setupClusters(map, context, places);
//    }
}
