package mapapp.bender.com.placesapp.repository.db.daos;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.Single;
import mapapp.bender.com.placesapp.repository.data.entities.places.Item;

import static android.arch.persistence.room.OnConflictStrategy.REPLACE;

@Dao
public interface PlacesDao {

    @Query("SELECT * FROM Item")
    LiveData<List<Item>> getAllPlaces();

    @Query("SELECT * FROM Item")
    Single<List<Item>> getAllPlacesRx();

    @Query("SELECT * FROM Item WHERE id = :id")
    LiveData<Item> getPlace(String id);

    @Query("SELECT * FROM Item WHERE id IN (:itemIds)")
    LiveData <List<Item>> getPlaces(List<Integer> itemIds);

    @Query("SELECT * FROM Item WHERE isFavorite")
    LiveData <List<Item>> getFavoritePlaces();

    @Insert(onConflict = REPLACE)
    void insert(Item item);

    @Insert(onConflict = REPLACE)
    void insertAll(List<Item> places);

    @Update(onConflict = REPLACE)
    void updatePlace(Item place);

    @Query("DELETE FROM Item")
    void deleteAll();

    @Query("DELETE FROM Item WHERE id = :placeId")
    void deletePlace(String placeId);
}
