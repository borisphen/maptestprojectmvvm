package mapapp.bender.com.placesapp.di.module;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import mapapp.bender.com.placesapp.Application;

@Module
public class ApplicationModule {

    private final Application app;

    public ApplicationModule(Application app) {
        this.app = app;
    }

    @Provides
    @Singleton
    public Application provideApp(){
        return app;
    }
}
