package mapapp.bender.com.placesapp;


import android.support.multidex.MultiDexApplication;

import com.facebook.stetho.Stetho;
import com.squareup.picasso.Picasso;

import javax.inject.Inject;

import mapapp.bender.com.placesapp.di.component.ApplicationComponent;
import mapapp.bender.com.placesapp.di.component.DaggerApplicationComponent;
import mapapp.bender.com.placesapp.di.module.ApplicationModule;
import mapapp.bender.com.placesapp.di.module.NetworkModule;


public class Application extends MultiDexApplication {
    private static Application instance;
    private ApplicationComponent applicationComponent;

    @Inject
    Picasso picasso;

    public static Application getInstance() {
        return instance;
    }

    public ApplicationComponent getApplicationComponent() {
        return applicationComponent;
    }

    @Override
    public void onCreate() {
        initDependencies();
        super.onCreate();
        instance = this;
        Stetho.initialize(
                Stetho.newInitializerBuilder(this)
                        .enableDumpapp(Stetho.defaultDumperPluginsProvider(this))
                        .enableWebKitInspector(Stetho.defaultInspectorModulesProvider(this))
                        .build());
        Picasso.setSingletonInstance(picasso);
    }

    private void initDependencies() {
        applicationComponent =
                DaggerApplicationComponent.builder()
                        .applicationModule(new ApplicationModule(this))
                        .networkModule(new NetworkModule())
                        .build();
        applicationComponent.inject(this);
    }

    public Picasso getPicasso() {
        return picasso;
    }
}
