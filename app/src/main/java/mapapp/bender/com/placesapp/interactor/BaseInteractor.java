package mapapp.bender.com.placesapp.interactor;

import javax.inject.Inject;

import mapapp.bender.com.placesapp.Application;
import mapapp.bender.com.placesapp.repository.db.AppDatabase;
import mapapp.bender.com.placesapp.repository.network.NetworkClient;

public abstract class BaseInteractor {
    @Inject
    AppDatabase database;

    protected NetworkClient networkController;

    protected Application context;

    public BaseInteractor(NetworkClient networkController, Application context) {
        this.networkController = networkController;
        this.context = context;
    }
}
