package mapapp.bender.com.placesapp.repository.data.entities.places;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class DataResponse {

    @SerializedName("pagesCount")
    @Expose
    private int pagesCount;
    @SerializedName("itemsCount")
    @Expose
    private int itemsCount;
    @SerializedName("items")
    @Expose
    private List<Item> items = new ArrayList<>();

    public int getPagesCount() {
        return pagesCount;
    }

    public void setPagesCount(int pagesCount) {
        this.pagesCount = pagesCount;
    }

    public int getItemsCount() {
        return itemsCount;
    }

    public void setItemsCount(int itemsCount) {
        this.itemsCount = itemsCount;
    }

    public List<Item> getItems() {
        return items;
    }

    public void setItems(List<Item> items) {
        this.items = items;
    }

}