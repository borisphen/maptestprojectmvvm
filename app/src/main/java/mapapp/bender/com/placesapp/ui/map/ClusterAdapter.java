package mapapp.bender.com.placesapp.ui.map;

import android.arch.lifecycle.LifecycleOwner;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bender.placesapp.R;
import com.bender.placesapp.databinding.InfoItemBinding;

import java.util.ArrayList;
import java.util.List;

import mapapp.bender.com.placesapp.repository.data.entities.places.Item;
import mapapp.bender.com.placesapp.utils.CollectionUtils;


public class ClusterAdapter extends RecyclerView.Adapter<ClusterAdapter.ViewHolder> {

    private List<Item> placeBindingModels = new ArrayList<>();
    private ClusterAdapterItemClickListener clickListener;
    private LifecycleOwner lifecycleOwner;

    public ClusterAdapter(ClusterAdapterItemClickListener clickListener, LifecycleOwner lifecycleOwner) {
        this.clickListener = clickListener;
        this.lifecycleOwner = lifecycleOwner;
    }

    @Override
    public ClusterAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        InfoItemBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),
                R.layout.info_item, parent, false);
        binding.executePendingBindings();
        return new ClusterAdapter.ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(ClusterAdapter.ViewHolder holder, int position) {
        holder.dataBinding.setLifecycleOwner(lifecycleOwner);
        holder.dataBinding.setModel(new PlaceBindingModel(placeBindingModels.get(position)));
        holder.dataBinding.setItemClicker(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clickListener.onClusterAdapterItemClick(placeBindingModels.get(position));
            }
        });
    }

    @Override
    public int getItemCount() {
        return placeBindingModels.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        InfoItemBinding dataBinding;

        ViewHolder(InfoItemBinding dataBinding) {
            super(dataBinding.getRoot());
            this.dataBinding = dataBinding;
        }
    }

    public void setData(List<Item> placeBindingModels) {
        this.placeBindingModels.clear();
        if (!CollectionUtils.isEmpty(placeBindingModels)) {
            this.placeBindingModels.addAll(placeBindingModels);
        }
        notifyDataSetChanged();
    }

    public interface ClusterAdapterItemClickListener {
        void onClusterAdapterItemClick(Item item);
    }
}
