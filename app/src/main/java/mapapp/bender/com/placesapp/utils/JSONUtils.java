package mapapp.bender.com.placesapp.utils;

import android.content.Context;

import com.google.gson.Gson;


public class JSONUtils {

    public static <T> T getObjectFromAsset(String asset, Class<T> classOfT, Context context) {
        String jsonString = FileUtils.getStringFromAsset(asset, context);

        return getObjectFromString(jsonString, classOfT);
    }

    public static <T> T getObjectFromString(String jsonString, Class<T> classOfT) {
        Gson gson = new Gson();
        T object = gson.fromJson(jsonString, classOfT);
        return object;
    }

    static public String getStringFromObject(Object object) {
        Gson gson = new Gson();
        String jsonRepresentation = gson.toJson(object);
        return jsonRepresentation;
    }
}
