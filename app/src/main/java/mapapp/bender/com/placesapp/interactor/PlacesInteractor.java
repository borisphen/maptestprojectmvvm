package mapapp.bender.com.placesapp.interactor;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.Single;
import io.reactivex.functions.Consumer;
import mapapp.bender.com.placesapp.Application;
import mapapp.bender.com.placesapp.di.scope.PerActivity;
import mapapp.bender.com.placesapp.repository.data.entities.places.Item;
import mapapp.bender.com.placesapp.repository.network.NetworkClient;

@PerActivity
public class PlacesInteractor extends BaseInteractor {

    @Inject
    public PlacesInteractor(NetworkClient networkClient, Application context) {
        super(networkClient, context);
    }

    public Observable<List<Item>> getPlaces(){
        return networkController.getItems();
    }
    public Single<List<Item>> getPlacesFromDbRx(){
        return database.getPlacesDao().getAllPlacesRx();
    }
}
