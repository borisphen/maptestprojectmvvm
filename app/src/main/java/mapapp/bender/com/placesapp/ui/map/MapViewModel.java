package mapapp.bender.com.placesapp.ui.map;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.support.annotation.NonNull;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import mapapp.bender.com.placesapp.interactor.PlacesInteractor;
import mapapp.bender.com.placesapp.repository.data.entities.places.Item;
import mapapp.bender.com.placesapp.repository.db.AppDatabase;
import mapapp.bender.com.placesapp.utils.CollectionUtils;


public class MapViewModel extends AndroidViewModel {

    private CompositeDisposable compositeDisposable = new CompositeDisposable();
    private MutableLiveData<Throwable> liveErrorHandler = new MutableLiveData<>();

    @Inject
    PlacesInteractor placesInteractor;

    @Inject
    AppDatabase database;

    public MapViewModel(@NonNull Application application) {
        super(application);
    }

    public LiveData<List<Item>> getMapData(){
        compositeDisposable.add(getMapDataFormDbRx()
                .subscribeOn(Schedulers.io())
                .doOnSuccess(items -> {
                    if (CollectionUtils.isEmpty(items)){
                        getDataFromSerer();
                    }
                }).subscribe());

        return getMapDataFormDb();
    }

    private void getDataFromSerer(){
        compositeDisposable.add(placesInteractor.getPlaces()
                .subscribeOn(Schedulers.io())
                .doOnNext(items -> database.getPlacesDao().insertAll(items))
                .observeOn(AndroidSchedulers.mainThread())
                .doOnError(e -> liveErrorHandler.postValue(e))
                .subscribe(items -> {}, error -> {}));
    }

    private LiveData<List<Item>> getMapDataFormDb(){
        return database.getPlacesDao().getAllPlaces();
    }

    private Single<List<Item>> getMapDataFormDbRx(){
        return placesInteractor.getPlacesFromDbRx();
    }

    @Override
    protected void onCleared() {
        compositeDisposable.clear();
        super.onCleared();
    }

    public LiveData<Throwable> getErrorData() {
        return liveErrorHandler;
    }
}
