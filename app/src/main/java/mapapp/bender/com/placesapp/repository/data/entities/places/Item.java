package mapapp.bender.com.placesapp.repository.data.entities.places;

import android.arch.persistence.room.Embedded;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import com.google.android.gms.maps.model.LatLng;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.google.maps.android.clustering.ClusterItem;

import mapapp.bender.com.placesapp.utils.JSONUtils;

@Entity
public class Item implements Place, ClusterItem{

    @NonNull
    @PrimaryKey
    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("name")
    @Expose
    private String name;
    @Embedded
    @SerializedName("location")
    @Expose
    private Location location;
    private String address;
    private int rate;
    private int distanceToMe;
    private int price;
    @Embedded
    private Owner owner;
    private boolean isFavorite;
    private String picture;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    @Override
    public double getLatitude() {
        return location.getLatitude();
    }

    @Override
    public double getLongitude() {
        return location.getLongitude();
    }

    @Override
    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Override
    public int getRate() {
        return rate;
    }

    public void setRate(int rate) {
        this.rate = rate;
    }

    @Override
    public int getDistanceToMe() {
        return distanceToMe;
    }

    @Override
    public String getOwnerFullName() {
        return owner.getFirstName() + " " + owner.getSurname();
    }

    public void setDistanceToMe(int distanceToMe) {
        this.distanceToMe = distanceToMe;
    }

    @Override
    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    @Override
    public String getPicture() {
        return picture;
    }

    public Owner getOwner() {
        return owner;
    }

    public void setOwner(Owner owner) {
        this.owner = owner;
    }

    @Override
    public String toString() {
        return "Item{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", location=" + location +
                ", address='" + address + '\'' +
                ", rate=" + rate +
                ", distanceToMe=" + distanceToMe +
                ", price=" + price +
                ", owner=" + owner +
                '}';
    }

    public void setFavorite(boolean favorite) {
        isFavorite = favorite;
    }

    @Override
    public boolean isFavorite() {
        return isFavorite;
    }

    @Override
    public LatLng getPosition() {
        return new LatLng(getLatitude(), getLongitude());
    }

    @Override
    public String getTitle() {
        return getName();
    }

    @Override
    public String getSnippet() {
        return JSONUtils.getStringFromObject(this);
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }
}
