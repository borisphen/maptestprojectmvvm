package mapapp.bender.com.placesapp.ui.map;

import android.arch.lifecycle.MutableLiveData;
import android.databinding.ObservableDouble;
import android.databinding.ObservableField;
import android.databinding.ObservableInt;

import mapapp.bender.com.placesapp.repository.data.entities.places.Place;
import mapapp.bender.com.placesapp.utils.MathUtils;

public class PlaceBindingModel {
    public final MutableLiveData<String> name = new MutableLiveData<>();
    public final MutableLiveData<String> picture = new MutableLiveData<>();
    public final MutableLiveData<String> distance = new MutableLiveData<>();
    public final MutableLiveData<String> address = new MutableLiveData<>();
    public final MutableLiveData<String> owner = new MutableLiveData<>();
    public final MutableLiveData<String> price = new MutableLiveData<>();
    public final MutableLiveData<Integer> rating = new MutableLiveData<>();

    public PlaceBindingModel(Place place) {
        name.postValue(place.getName());
        distance.postValue(place.getDistanceToMe() + " km");
        address.postValue(place.getAddress());
        owner.postValue(place.getOwnerFullName());
        price.postValue(place.getPrice() + "$/per day");
        rating.postValue(place.getRate());
        picture.postValue(place.getPicture());
    }
}
