package mapapp.bender.com.placesapp.di.component;

import dagger.Subcomponent;
import mapapp.bender.com.placesapp.di.module.ActivityModule;
import mapapp.bender.com.placesapp.di.scope.PerActivity;
import mapapp.bender.com.placesapp.ui.map.MapViewModel;
import mapapp.bender.com.placesapp.ui.map.MapsActivity;

@PerActivity
@Subcomponent(modules = {ActivityModule.class})
public interface ActivitySubComponent {
    void inject(MapsActivity mapsActivity);
    void inject(MapViewModel viewModel);
}
