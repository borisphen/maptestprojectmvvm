package mapapp.bender.com.placesapp.repository.network;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.util.Log;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import mapapp.bender.com.placesapp.Application;
import mapapp.bender.com.placesapp.repository.data.entities.places.DataResponse;
import mapapp.bender.com.placesapp.repository.data.entities.places.Item;
import mapapp.bender.com.placesapp.repository.data.entities.places.Owner;
import mapapp.bender.com.placesapp.utils.JSONUtils;
import mapapp.bender.com.placesapp.utils.MathUtils;

public class NetworkClient {

    private static final int MAX_PEOPLE_AMOUNT = 500;
    private static final String MOCK_GEO_DATA_FILE = "map.json";

    private Api api;
    private Context context;

    @Inject
    public NetworkClient(Application context, Api api) {
        this.context = context;
        this.api = api;
    }

    private void populateFakeData(List<Item> items) throws IOException {
        for (int i = 0; i < items.size(); i++) {
            //too slow
//            populateAddress(items.get(i));
            items.get(i).setAddress("Zurich Strasse# " + i);
            items.get(i).setRate(MathUtils.randomInt(1, 5));
            items.get(i).setPrice(MathUtils.randomInt(39, 99));
            items.get(i).setDistanceToMe(MathUtils.randomInt(1, 17));
            items.get(i).setPicture("https://picsum.photos/150/100/?image=" + MathUtils.randomInt(0, 1000));
        }
    }

    private void populateAddress(Item item) throws IOException {
        Geocoder geocoder = new Geocoder(context, Locale.getDefault());

        List<Address> addresses = geocoder.getFromLocation(item.getLatitude(), item.getLongitude(), 1);

        String city = addresses.get(0).getLocality();
        String state = addresses.get(0).getAdminArea();
        String zip = addresses.get(0).getPostalCode();
        String country = addresses.get(0).getCountryName();

        item.setAddress(city + " " + state + " " + zip + " " + country);
        Log.d("Addressed", item.toString());
    }

    private DataResponse getDataResponse() {
        return JSONUtils.getObjectFromAsset(MOCK_GEO_DATA_FILE, DataResponse.class, context);
    }

    public Observable<List<Item>> getItems() {
        List<Item> itemsList = getDataResponse().getItems();
        int size = itemsList.size();
        return Observable.fromIterable(itemsList)
                .toList()
                .map(items -> {
                    populateFakeData(items);
                    return items;
                })
                .toObservable()
                .zipWith(getNames(size), ((items, u) -> {
                    for (int i = 0; i < items.size(); i++) {
                        items.get(i).setOwner(u.get(i));
                    }
                    return items;
                }));
    }

    private Observable<List<Owner>> getNames(int amount) {
        int pages = amount / MAX_PEOPLE_AMOUNT;
        int rest = amount % MAX_PEOPLE_AMOUNT;
        List<Observable<List<Owner>>> list = new ArrayList<>();
        for (int i = 1; i <= pages; i++) {
            list.add(api.getOwner(MAX_PEOPLE_AMOUNT));
        }
        list.add(api.getOwner(rest));
        Log.d("PAGES", "pages = " + list.size());
        return Observable.fromIterable(list)
                .flatMap(owners -> owners.observeOn(Schedulers.computation()))
                .subscribeOn(Schedulers.io())
                .toList()
                .map(items -> {
                    List<Owner> allOwners = new ArrayList<>();
                    for (List<Owner> page : items) {
                        allOwners.addAll(page);
                    }
                    return allOwners;
                }).toObservable();
    }
}
