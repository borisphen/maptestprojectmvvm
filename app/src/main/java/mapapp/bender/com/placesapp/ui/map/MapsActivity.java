package mapapp.bender.com.placesapp.ui.map;

import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.bender.placesapp.R;
import com.bender.placesapp.databinding.ActivityMapsBinding;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;

import java.util.List;

import mapapp.bender.com.placesapp.di.module.ActivityModule;
import mapapp.bender.com.placesapp.repository.data.entities.places.Item;
import mapapp.bender.com.placesapp.utils.CollectionUtils;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback
        , MapController.OnClusterClickListener {

    private GoogleMap map;
    private MapViewModel viewModel;
    private MapController mapController;
    private ActivityMapsBinding binding;
    private ClusterAdapter clusterAdapter;
    private RecyclerView rvCluster;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_maps);
        rvCluster = binding.rvCluster;
        viewModel = ViewModelProviders.of(this).get(MapViewModel.class);
        Log.i("VIEW_MODEL", "Address = " + viewModel);
        setupAdapter();
        mapapp.bender.com.placesapp.Application.getInstance().getApplicationComponent().plus(new ActivityModule(this)).inject(viewModel);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    private void getData() {
        viewModel.getErrorData().observe(this, e -> Toast.makeText(this, "Oops something goes wrong: " + e.getLocalizedMessage(),
                Toast.LENGTH_LONG).show());
        viewModel.getMapData().observe(this, items -> {
            Log.d("FROM_DB", items.toString());
            if (!CollectionUtils.isEmpty(items)) {
                processWithMap(items);
            }
        });
    }

    private void processWithMap(List<Item> items) {
        mapController = new MapController(map, this, this);
        mapController.setupClusters(items);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;
        getData();
    }

    @Override
    public void OnClusterClick(List<Item> items) {
        clusterAdapter.setData(items);
        rvCluster.setVisibility(View.VISIBLE);
    }

    private void setupAdapter() {
        clusterAdapter = new ClusterAdapter(item -> mapController.showInfoWindow(item), this);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        rvCluster.setLayoutManager(layoutManager);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(rvCluster.getContext(),
                layoutManager.getOrientation());
        rvCluster.addItemDecoration(dividerItemDecoration);
        rvCluster.setAdapter(clusterAdapter);
        initSwipeToHide();
    }

    private void initSwipeToHide(){
        ItemTouchHelper.SimpleCallback simpleCallback =
                new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.DOWN) {
                    @Override
                    public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder,
                                          RecyclerView.ViewHolder target) {
                        return false;
                    }

                    @Override
                    public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
                        clusterAdapter.setData(null);
                        rvCluster.setVisibility(View.GONE);
                    }
                };

        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(simpleCallback);
        itemTouchHelper.attachToRecyclerView(rvCluster);
    }
}
